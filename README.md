# README #

AngularJS : La manipulation des données
Dynamisez votre application web avec les bases de données, les utilisateurs et les API

https://www.video2brain.com/fr/formation/angularjs-la-manipulation-des-donnees

## Table des matières

### Aborder la manipulation des données avec AngularJS
* Démarrer avec les fichiers à disposition
* Découvrir les prérequis pour bien suivre la formation
* Utiliser les fichiers source

### Gérer une base de données Cloud
* Découvrir le service Firebase
* Rendre disponible angularFire au sein de l'application
* Trouver et installer d'autres modules via Bower
* Insérer des données dans Firebase
* Récupérer et afficher les données stockées dans Firebase
* Ajouter des données à une collection
* Supprimer des données de la collection
* Mettre à jour des données

### S'identifier avec Firebase et AngularJS
* Créer des utilisateurs pour travailler
* Écrire un formulaire de création d'utilisateurs
* Persister les informations de l'utilisateur
* Créer un formulaire pour authentifier un utilisateur
* Utiliser la session pour interagir avec l'interface
* Restreindre l'accès aux routes

### Découvrir le serveur REST, $http, $resource et Restangular
* Découvrir le serveur REST
* Voir un exemple de callback
* Connaître l'utilité des ressources et de Restangular

### Tester son application et profiter d'une vision fonctionnelle et unitaire
* Installer le framework Karma et préparer le test
* Créer et lancer le test avec son IDE
* Installer DalekJS, écrire et lancer le test
* Conclure sur la manipulation des données avec AngularJS
