/**
 * Example of directive's definition, must be renamed or removed
 *
 * @author Julien Moulin <julien@baliz.org>
 */

'use strict';

app.directive('decorateItem', [function () {
	return {
		// restrict: 'A',
		template: '{{ item.name }} dans la catégorie {{ item.category_id }}'
	};
}])