/**
 * Example of service's definition, must be renamed or removed
 *
 * @author Julien Moulin <julien@baliz.org>
 */

'use strict';

var items = [
	{ 'name' : 'Eyes Wide Shut', 'category_id' : 1  },
	{ 'name' : 'Orange Mécanique', 'category_id' : 1 },
	{ 'name' : 'Spartacus', 'category_id' : 3 }
];

app.service('itemProvider', function () {
	this.getItems = function () {
		return items;
	}

	this.create = function(item) {
		items.push(item);
		return items; // Retourne les items mis à jour.
	}

});