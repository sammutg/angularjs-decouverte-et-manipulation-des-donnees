/**
 * Example of service's definition, must be renamed or removed
 *
 * @author Julien Moulin <julien@baliz.org>
 */

'use strict';

var categories = [
	{ 'id' : 1, 'name' : 'Films'  },
	{ 'id' : 2, 'name' : 'Musiques' },
	{ 'id' : 3, 'name' : 'Livres' }
];

app.service('categoryProvider', [function () {
	this.getCategories = function () {
		return categories;
	};
}]);


