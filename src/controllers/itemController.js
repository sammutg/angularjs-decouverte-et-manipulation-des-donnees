/**
 * Example of controller's definition, must be renamed or removed
 *
 * @author Julien Moulin <julien@baliz.org>
 */

'use strict';

	// Un controler une action

	// ITEM INDEX
	app.controller('ItemIndexController', ['$scope', '$rootScope', function ($scope, $rootScope) {
		$rootScope.test = "Variable du Ctrl ItemIndexController";
	}]);

	// ITEM LIST
	app.controller('ItemListController', ['$scope', 'itemProvider', function ($scope, itemProvider) {
		// $scope.items = items;
		$scope.items = itemProvider.getItems();
	}]);

	// ITEM CREATE
	app.controller('ItemCreateController', ['$scope', 'categoryProvider', 'itemProvider',  function ($scope, categoryProvider, itemProvider) {

		// Récupération des items
		$scope.items = itemProvider.getItems();

		// Récupération des catégories
		$scope.categories = categoryProvider.getCategories();

		// Ajout d'un item
		$scope.createItem = function(item) {
			$scope.items = itemProvider.create(item);
		}

	}]);

	// ITEM REMOVE
	app.controller('ItemRemoveController', ['$scope', function ($scope) {
	}]);
