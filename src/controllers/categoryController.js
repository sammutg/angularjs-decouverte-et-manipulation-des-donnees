/**
 * Example of controller's definition, must be renamed or removed
 *
 * @author Julien Moulin <julien@baliz.org>
 */

'use strict';

	// Un controler une action

	// CATEGORY INDEX
	app.controller('CategoryIndexController', ['$scope', function ($scope) {
		$scope.test = "Variable du Ctrl CategoryIndexController";
	}]);

	// CATEGORY LIST
	app.controller('CategoryListController', ['$scope', function ($scope) {
	}]);

	// CATEGORY CREATE
	app.controller('CategoryCreateController', ['$scope', function ($scope) {
	}]);

	// CATEGORY REMOVE
	app.controller('CategoryRemoveController', ['$scope', function ($scope) {
	}]);
