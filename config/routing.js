/**
 * Routing definitions
 *
 * @author Julien Moulin <julien@baliz.org>
 */

'use strict';

app.config( function ($routeProvider) {
	$routeProvider
		.when('/list', {
			controller: 'ItemListController',
			templateUrl: 'views/list.html'
		})
		.when('/item/new', {
			controller: 'ItemCreateController',
			templateUrl: 'views/create.html'
		})
		.when('/', {
			controller: 'ItemIndexController',
			templateUrl: 'views/dashboard.html'
		})
		.otherwise({
			redirectTo: '/',
		});
});