module.exports = {

    // 'Page title is correct': function (test) {
    //   test
    //     .open('http://google.fr')
    //     .assert.title().is('Google', 'It has title')
    //     .done();
    //   },

    // 'Amazon does its thing': function (test) {
    //   test
    //     .open('http://www.amazon.fr/')
    //     .type('#twotabsearchtextbox', 'Blues Brothers')
    //     .click('.nav-submit-input')
    //     .waitForElement('#result_0')
    //     .assert.text('#result_0 .newaps a span').is('The Blues Brothers')
    //     .done();
    // },

    'Collectify': function (test) {
      test
        .open('http://collectify.io/app.html')
        // .assert.exists('a[href = "#/item/new"]',    'Create new item link exist')
        .assert.exists('a[href="#/list"]', 'List item link exist')
        .click('a[href="#/list"]')
        .waitForElement('.container')
        .screenshot('collectify-list.png')
        .done();
    }

};